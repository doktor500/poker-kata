require_relative '../lib/suits'

describe Suits do
	
	class HandSuits 
		
		include Suits

		attr_reader :suits
		
		def initialize(*suits)
			@suits = suits
		end 
	end

	it 'returns if ranks form a straight are valid' do
		:given
		equal_suits = HandSuits.new Suits::DIAMONDS, Suits::DIAMONDS
		non_equal_suits = HandSuits.new Suits::DIAMONDS, Suits::CLUBS

		:expect
		equal_suits.all_suits_are_equal?.should == true
		non_equal_suits.all_suits_are_equal?.should == false
	end
end