require_relative '../lib/hand'
require_relative '../lib/card'
require_relative '../lib/suits'
require_relative '../lib/ranks'

describe Hand do
	it 'returns if the hand is straight flush' do
        :given
        hand = Hand.new(
            Card.new(Suits::DIAMONDS, Ranks::KING),
            Card.new(Suits::DIAMONDS, Ranks::JACK),
            Card.new(Suits::DIAMONDS, Ranks::QUEEN),
            Card.new(Suits::DIAMONDS, Ranks::TEN),
            Card.new(Suits::DIAMONDS, Ranks::ACE)
        )

        :expect
        hand.name.should == :straight_flush
  	end

  	it 'returns if the hand is four of a kind' do
        :given
        hand = Hand.new(
            Card.new(Suits::SPADES,   Ranks::KING),
            Card.new(Suits::HEARTS,   Ranks::KING),
            Card.new(Suits::DIAMONDS, Ranks::KING),
            Card.new(Suits::CLUBS,    Ranks::KING),
            Card.new(Suits::SPADES,   Ranks::SEVEN)
        )

        :expect
        hand.name.should == :four_of_a_kind
  	end

  	it 'returns if the hand is full house' do
        :given
        hand = Hand.new(
            Card.new(Suits::SPADES,   Ranks::KING),
            Card.new(Suits::HEARTS,   Ranks::KING),
            Card.new(Suits::DIAMONDS, Ranks::KING),
            Card.new(Suits::CLUBS,    Ranks::SEVEN),
            Card.new(Suits::SPADES,   Ranks::SEVEN)
        )

        :expect
        hand.name.should == :full_house
  	end

	it 'returns if the hand is flush' do
        :given
    	hand = Hand.new(
            Card.new(Suits::DIAMONDS, Ranks::SIX),
            Card.new(Suits::DIAMONDS, Ranks::EIGHT),
            Card.new(Suits::DIAMONDS, Ranks::JACK),
            Card.new(Suits::DIAMONDS, Ranks::TWO),
            Card.new(Suits::DIAMONDS, Ranks::FOUR)
        )

        :expect
        hand.name.should == :flush
  	end

  	it 'returns if the hand is straight' do
        :given
        hand = Hand.new(
            Card.new(Suits::SPADES,   Ranks::TWO),
            Card.new(Suits::SPADES,   Ranks::FOUR),
            Card.new(Suits::DIAMONDS, Ranks::FIVE),
            Card.new(Suits::CLUBS,    Ranks::THREE),
            Card.new(Suits::CLUBS,    Ranks::SIX)
        )

        :expect
        hand.name.should == :straight
  	end

  	it 'returns if the hand is three of a kind' do
        :given
        hand = Hand.new(
            Card.new(Suits::SPADES,   Ranks::TWO),
            Card.new(Suits::CLUBS,    Ranks::TWO),
            Card.new(Suits::DIAMONDS, Ranks::TWO),
            Card.new(Suits::CLUBS,    Ranks::THREE),
            Card.new(Suits::CLUBS,    Ranks::SEVEN)
        )
        
        :expect
        hand.name.should == :three_of_a_kind
  	end

  	it 'returns if the hand is two pair' do
        :given
    	hand = Hand.new(
            Card.new(Suits::SPADES,   Ranks::TWO),
            Card.new(Suits::CLUBS,    Ranks::TWO),
            Card.new(Suits::DIAMONDS, Ranks::THREE),
            Card.new(Suits::CLUBS,    Ranks::THREE),
            Card.new(Suits::CLUBS,    Ranks::SEVEN)
        )
        
        :expect
        hand.name.should == :two_pair
  	end

  	it 'returns if the hand is one pair' do
        :given
        hand = Hand.new(
            Card.new(Suits::SPADES,   Ranks::TWO),
            Card.new(Suits::CLUBS,    Ranks::TWO),
            Card.new(Suits::DIAMONDS, Ranks::THREE),
            Card.new(Suits::CLUBS,    Ranks::JACK),
            Card.new(Suits::CLUBS,    Ranks::SEVEN)
        )

        :expect
        hand.name.should == :one_pair
  	end

  	it 'returns if the hand is high card' do
        :given
    	hand = Hand.new(
            Card.new(Suits::DIAMONDS, Ranks::KING),
            Card.new(Suits::DIAMONDS, Ranks::ACE),
            Card.new(Suits::SPADES,   Ranks::TWO),
            Card.new(Suits::SPADES,   Ranks::THREE),
            Card.new(Suits::HEARTS,   Ranks::FOUR)  
        )
        
        :expect
        hand.name.should == :high_card
  	end
end