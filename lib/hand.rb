class Hand

	include Suits
	include Ranks

	attr_reader :suits, :ranks

	def initialize(*cards)
		@suits = cards.map { |card| card.suit }.sort
		@ranks = cards.map { |card| card.rank }.sort
	end

	def name
		poker_hands.find { |hand_name, hand| hand.call }.first
	end

	private

	def poker_hands
		{
			straight_flush:  lambda { all_suits_are_equal? and ranks_goes_from_ten_to_ace? },
			four_of_a_kind:  lambda { ranks_grouped_are_equal_to? [1, 4] },
			full_house:      lambda { ranks_grouped_are_equal_to? [2, 3] },
			flush:           lambda { all_suits_are_equal? },
			straight:        lambda { ranks_form_a_straight? },
			three_of_a_kind: lambda { ranks_grouped_are_equal_to? [1, 1, 3] },
			two_pair:        lambda { ranks_grouped_are_equal_to? [1, 2, 2] },
			one_pair:        lambda { ranks_grouped_are_equal_to? [1, 1, 1, 2] },
			high_card:       lambda { true }
		}
	end
end