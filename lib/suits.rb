module Suits
	
	SPADES   = '♠'
	HEARTS   = '♥'
	DIAMONDS = '♦'
	CLUBS    = '♣'

	def all_suits_are_equal?
		suits.uniq.size == 1
	end
end