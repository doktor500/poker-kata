module Ranks
	
	ACE   = 'A'
	TWO   = '2'
	THREE = '3'
	FOUR  = '4'
	FIVE  = '5'
	SIX   = '6'
	SEVEN = '7'
	EIGHT = '8'
	NINE  = '9'
	TEN   = '10'
	JACK  = 'J'
	QUEEN = 'Q'
	KING  = 'K'

	def ranks_form_a_straight?
		Ranks.all_possible_straights.include? ranks
	end

	def ranks_goes_from_ten_to_ace?
		[Ranks::TEN, Ranks::JACK, Ranks::QUEEN, Ranks::KING, Ranks::ACE].to_set == ranks.to_set
	end

	def ranks_grouped_are_equal_to? grouped_ranks
		group_ranks.values.sort == grouped_ranks.sort
	end

	private

	def self.all_possible_straights 
		all_ranks = constants.map { |rank| const_get(rank) }
		(all_ranks << ACE).each_cons(5) 
	end

	def group_ranks
		ranks.inject(Hash.new(0)) do |grouped_ranks, rank| 
			grouped_ranks[rank] += 1
			grouped_ranks
		end
	end
end